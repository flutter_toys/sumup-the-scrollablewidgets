# Summary of Scrollable Widgets

This project is the result of a data survey to identify and select specific scrollable widgets.

### Index
[1. SingleChildScrollView ](#1-singlechildscrollview)

[2. ListView](#2-listview)

[3. GridView](#3-gridview)

[4. ReorderableListView](#4-reorderablelistview)

[5. CustomScrollView](#5-customscrollview)

# 1. SingleChildScrollView

A box in which a single widget can be scrolled.
This widget is useful when you have a single box that will normally be entirely visible, for example **a clock face in a time picker**, but you need to make sure it can be scrolled if the container gets too small in one axis (the scroll direction).

It is also useful if you need to shrink-wrap in both axes (the main scrolling direction as well as the cross axis), as one might see in a **dialog or pop-up menu**. 

<details>
  <summary>Application snapshot</summary>
  <img src="https://gitlab.com/flutter_toys/sumup-the-scrollablewidgets/uploads/dba9656d5f74ea482d26f3412fb2355d/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-02-15_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_3.15.56.png" width = "50%"name="image-name">
  <img src="https://gitlab.com/flutter_toys/sumup-the-scrollablewidgets/uploads/8da3282b7ac4a3431c97f5c93f8a4bc0/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-02-15_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_3.16.07.png" width = "50%" name="image-name">
</details>

# 2. ListView

ListView is the most commonly used scrolling widget. It displays its children one after another in the scroll direction. In the cross axis, the children are required to fill the ListView.

What makes sense here is the difference between the list view and the list view **using the Builder**.

The **default list view** only loads all child values at once, but for **the Builder**, only values within the scroll range are loaded, and then additionally, depending on the scroll position.

**This allows you to create smoother applications, both memory and rendering!!**

<details>
  <summary>Application snapshot</summary>
  <img src="https://gitlab.com/flutter_toys/sumup-the-scrollablewidgets/uploads/433461191e06b4662cb86983921ee820/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-02-15_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_3.32.51.png" width = "50%"name="image-name">
  <img src="https://gitlab.com/flutter_toys/sumup-the-scrollablewidgets/uploads/6daeca806417c9670e7d650fefd3a94c/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-02-15_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_3.32.59.png" width = "50%" name="image-name">
  <img src="https://gitlab.com/flutter_toys/sumup-the-scrollablewidgets/uploads/2f1cd95ab38fe15e0da9068787b08bb2/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-02-15_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_3.33.11.png" width = "50%" name="image-name">
</details>

# 3. GridView

The main axis direction of a grid is the direction in which it scrolls (the scrollDirection).

The most commonly used grid layouts are **GridView.count**, which creates a layout with a fixed number of tiles in the cross axis, and GridView.extent, which creates a layout with tiles that have a maximum cross-axis extent. A custom SliverGridDelegate can produce **an arbitrary 2D arrangement of children**, including arrangements that are unaligned or overlapping.

As with list views, you can create more details through **the Builder**.

<details>
  <summary>Application snapshot</summary>
  <img src="https://gitlab.com/flutter_toys/sumup-the-scrollablewidgets/uploads/04116c0a3661e0f53b48b335594cf8a4/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-02-15_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_3.44.42.png" width = "50%"name="image-name">
</details>

# 4. ReorderableListView

A scrolling container that allows the user to interactively reorder the list items.

It is the only **Stateful widget** implemented here, and I was surprised that the widget can be repositioned directly through drag-and-drop. 
It was like a widget specialized in **user interaction** such as puzzles and chess.

<details>
  <summary>Application snapshot</summary>
  <img src="https://gitlab.com/flutter_toys/sumup-the-scrollablewidgets/uploads/a02c0435d2e54f6caf40f54183d5d2db/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-02-15_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_3.54.51.png" width = "50%"name="image-name">
</details>

# 5. CustomScrollView

A ScrollView that creates custom scroll effects using slivers.

A CustomScrollView lets you supply slivers directly to create various scrolling effects, such as lists, grids, and expanding headers. For example, to create a scroll view that contains an expanding app bar followed by a list and a grid, use a list of three slivers: SliverAppBar, SliverList, and SliverGrid.

Although I was a little reluctant to create a new class, I thought it would be right to adopt this widget casue of its high degree of freedom.

<details>
  <summary>Application snapshot</summary>
  <img src="https://gitlab.com/flutter_toys/sumup-the-scrollablewidgets/uploads/747b0a2a76382b5affc34fe86e209c3f/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-02-15_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_4.03.50.png" width = "50%"name="image-name">
</details>
